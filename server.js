const { response } = require("express");
const express = require("express");
const app = express();
const port = 5000;

app.use(express.json());
app.use(express.json({ extended: false }))

let users = [];

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

app.get("/backend", (req, res) => {
  res.writeHead(200, {
    'Content-Type': 'application/json'
  });
  res.end(JSON.stringify(users));
});

app.post('/backend', (req, res) => {
  const user = req.body;
  users.push(user)
  res.status(201).send("Created")
})

app.delete('/backend/:id', (req, res) => {
  let { id } = req.params
  users = users.filter(user => user.id !== id)
  res.status(204).send("Deleted user")
})