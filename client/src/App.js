import React, { useEffect, useState } from "react";
import "./App.css";
import Person from "./component/Person";

function App() {
  return (
    <div className="App">
      <h1>Timer</h1>
      <Person />
      <hr />
    </div>
  );
}

export default App;
